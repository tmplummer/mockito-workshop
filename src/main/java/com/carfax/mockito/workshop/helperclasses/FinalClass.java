package com.carfax.mockito.workshop.helperclasses;

public final class FinalClass {

    public String getSomething() {
        return "something";
    }

    public String getSomethingElse() {
        return "something else";
    }

    public void doSomething(String stringValue, int intValue) {

    }
}
