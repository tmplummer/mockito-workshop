package com.carfax.mockito.workshop.helperclasses;

import java.util.Date;

public class FakeDao {

    public void storeDate(Date date) {

    }

    public void storeRecord(HistoryRecord record) {

    }

    public void update(HistoryRecord record) {

    }

    public HistoryRecord findFirst() {
        return null;
    }
}
