package com.carfax.mockito.workshop.helperclasses;

import java.util.Date;

public class HistoryRecord {
    private String vin;
    private Date tranDate;
    private String description;
    private Date lastModifiedDate;

    public HistoryRecord(String vin, Date tranDate, String description) {
        this.vin = vin;
        this.tranDate = tranDate;
        this.description = description;
    }

    public String getVin() {
        return vin;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoryRecord that = (HistoryRecord) o;

        if (!vin.equals(that.vin)) return false;
        if (!tranDate.equals(that.tranDate)) return false;
        if (!description.equals(that.description)) return false;
        return lastModifiedDate != null ? lastModifiedDate.equals(that.lastModifiedDate) : that.lastModifiedDate == null;
    }

    @Override
    public int hashCode() {
        int result = vin.hashCode();
        result = 31 * result + tranDate.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HistoryRecord{" +
                "vin='" + vin + '\'' +
                ", tranDate=" + tranDate +
                ", description='" + description + '\'' +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
