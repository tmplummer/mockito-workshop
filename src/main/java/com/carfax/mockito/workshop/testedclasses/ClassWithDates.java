package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;
import com.carfax.mockito.workshop.helperclasses.HistoryRecord;

import java.util.Date;

public class ClassWithDates {

    public void storeDate(FakeDao dao) {
        dao.storeDate(new Date());
    }

    public void storeHistoryRecord(FakeDao dao) {
        HistoryRecord historyRecord = new HistoryRecord("vin", new Date(0), "description");
        historyRecord.setLastModifiedDate(new Date());
        dao.storeRecord(historyRecord);
    }
}
