package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;
import com.carfax.mockito.workshop.helperclasses.FinalClass;
import com.carfax.mockito.workshop.helperclasses.HistoryRecord;

import java.util.Date;
import java.util.Random;

public class DebuggingMockitoProblems {

    private FinalClass finalClass;

    public DebuggingMockitoProblems(FinalClass finalClass) {
        this.finalClass = finalClass;
    }

    public void storeRecord(FakeDao dao, HistoryRecord record) {
        //TODO: make sure we set the lastModifiedDate before storing the record
        dao.storeRecord(record);
        record.setDescription("new description");
    }

    public void storeThenUpdateRecord(FakeDao dao) {
        HistoryRecord record = new HistoryRecord("vin", new Date(0), "description");
        dao.storeRecord(record);
        record.setDescription("new description2");
        dao.update(record);
    }

    public String getSomethingFromFinalClass() {
        return finalClass.getSomething();
    }

    public String getSomethingElseFromFinalClass() {
        return finalClass.getSomethingElse();
    }

    public void doSomething() {
        finalClass.doSomething("hello", new Random().nextInt());
    }
}
