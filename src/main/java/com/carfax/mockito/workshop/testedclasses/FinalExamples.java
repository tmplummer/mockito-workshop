package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FinalClass;

public class FinalExamples {
    private FinalClass finalClass = new FinalClass();

    public String getSomethingFromFinalClass() {
        return finalClass.getSomething();
    }

}
