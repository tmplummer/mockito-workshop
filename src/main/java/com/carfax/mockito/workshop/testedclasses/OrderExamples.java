package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.OrderThing;

import java.util.Random;

public class OrderExamples {

    public void doThingsInTheCorrectOrder(OrderThing...orderThings) {
        for (OrderThing orderThing : orderThings) {
            orderThing.first();
            orderThing.second();
            orderThing.third();
        }
    }

    public void doThingsInTheCorrectButDifferentOrder(OrderThing...orderThings) {
        for (OrderThing orderThing : orderThings) {
            orderThing.first();
        }
        for (OrderThing orderThing : orderThings) {
            orderThing.second();
        }
        for (OrderThing orderThing : orderThings) {
            orderThing.third();
        }
    }

    public void doThingsInCorrectButRandomOrder(OrderThing...orderThings) {
        if (new Random().nextBoolean()) {
            doThingsInTheCorrectOrder(orderThings);
        } else {
            doThingsInTheCorrectButDifferentOrder(orderThings);
        }
    }

    public void doThings(OrderThing thing1) {
        thing1.first();
        thing1.first();
        thing1.second();
    }

    public void doNothingWith(OrderThing thing) {
    }

    public void doOnlyFirstWith(OrderThing ... things) {
        for (OrderThing thing: things) {
            thing.first();
        }
    }
}
