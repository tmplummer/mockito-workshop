package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;

import java.util.Date;

public class LazyVerificationExample {
    private FakeDao fakeDao;

    public LazyVerificationExample(FakeDao fakeDao) {
        this.fakeDao = fakeDao;
    }

    public void storeMany(Date... dates) {
        for (Date date : dates) {
            fakeDao.storeDate(date);
        }
    }
}
