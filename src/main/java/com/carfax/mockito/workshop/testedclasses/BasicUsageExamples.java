package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;
import com.carfax.mockito.workshop.helperclasses.HistoryRecord;

import java.io.*;

public class BasicUsageExamples {
    private FakeDao dao;

    public BasicUsageExamples(FakeDao dao) {
        this.dao = dao;
    }

    public HistoryRecord findFirstHistoryRecord() {
        return dao.findFirst();
    }

    public void storeRecord(HistoryRecord historyRecord) {
        dao.storeRecord(historyRecord);
    }

    public String readFully(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        bufferedReader.close();
        return stringBuilder.toString();
    }
}
