package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;
import com.carfax.mockito.workshop.helperclasses.HistoryRecord;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class BasicUsageExamplesTest {
    private FakeDao dao = mock(FakeDao.class);
    private BasicUsageExamples testSubject = new BasicUsageExamples(dao);

    @Test
    public void stubAClass() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        doReturn(toBeReturned).when(dao).findFirst();

        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
    }

    @Test
    public void stubAClassThePreferredWay() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        when(dao.findFirst()).thenReturn(toBeReturned);

        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
    }

    @Test
    public void stubAClassWithExceptionOnCall() {
        RuntimeException toBeThrown = new RuntimeException();
        when(dao.findFirst()).thenThrow(toBeThrown);

        try {
            testSubject.findFirstHistoryRecord();
            fail("Should have blown up");
        } catch (RuntimeException e) {
            assertSame(toBeThrown, e);
        }
    }

    @Test
    public void verifyBehaviorWithAnyAndClassType() {
        HistoryRecord historyRecord = new HistoryRecord("vin", new Date(), "description");

        testSubject.storeRecord(historyRecord);

        verify(dao).storeRecord(any(HistoryRecord.class));
    }

    @Test
    public void verifyBehaviorWithAnyButWithoutClassType() {
        HistoryRecord historyRecord = new HistoryRecord("vin", new Date(), "description");

        testSubject.storeRecord(historyRecord);
        testSubject.storeRecord(null);

        verify(dao, times(2)).storeRecord(any());
    }

    @Test
    public void verifyBehaviorWithNullable() {
        HistoryRecord historyRecord = new HistoryRecord("vin", new Date(), "description");

        testSubject.storeRecord(historyRecord);
        testSubject.storeRecord(null);

        verify(dao, times(2)).storeRecord(nullable(HistoryRecord.class));
    }

    @Test
    public void verifyBehaviorExpectingNullValue() {
        testSubject.storeRecord(null);

        verify(dao).storeRecord(isNull());
    }

    @Test
    public void verifyBehaviorNormallyWhichUsesEquals() {
        HistoryRecord historyRecord = new HistoryRecord("vin", new Date(), "description");

        testSubject.storeRecord(historyRecord);

        verify(dao).storeRecord(historyRecord);
    }

    @Test
    public void verifyBehaviorWithSame() {
        HistoryRecord historyRecord = new HistoryRecord("vin", new Date(), "description");

        testSubject.storeRecord(historyRecord);

        verify(dao).storeRecord(same(historyRecord));
    }

    @Test
    public void stubAClassForMultipleCalls() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        doReturn(toBeReturned).when(dao).findFirst();

        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
    }

    @Test
    public void overwriteStubAction() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        HistoryRecord toBeReturned2 = new HistoryRecord("vin", new Date(), "description");
        when(dao.findFirst()).thenReturn(toBeReturned);
        when(dao.findFirst()).thenReturn(toBeReturned2);

        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
    }

    @Test
    public void stubAClassForMultipleCallsWithDifferentValues() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        HistoryRecord toBeReturned2 = new HistoryRecord("vin", new Date(), "description");
        doReturn(toBeReturned, toBeReturned2).when(dao).findFirst();

        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
    }

    @Test
    public void stubAClassForMultipleCallsWithDifferentValuesThePreferredWay() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        HistoryRecord toBeReturned2 = new HistoryRecord("vin", new Date(), "description");
        when(dao.findFirst()).thenReturn(toBeReturned, toBeReturned2);

        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
    }

    @Test
    public void stubAClassForMultipleCallsWithDifferentTypesOfActions() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        HistoryRecord toBeReturned2 = new HistoryRecord("vin", new Date(), "description");
        RuntimeException toBeThrown = new RuntimeException();
        doReturn(toBeReturned, toBeReturned2).doThrow(toBeThrown).when(dao).findFirst();

        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
        try {
            testSubject.findFirstHistoryRecord();
            fail("Should have blown up");
        } catch (RuntimeException e) {
            assertSame(toBeThrown, e);
        }
    }

    @Test
    public void stubAClassForMultipleCallsWithDifferentTypesOfActionsThePreferredWay() {
        HistoryRecord toBeReturned = new HistoryRecord("vin", new Date(), "description");
        HistoryRecord toBeReturned2 = new HistoryRecord("vin", new Date(), "description");
        RuntimeException toBeThrown = new RuntimeException();
        when(dao.findFirst()).thenReturn(toBeReturned, toBeReturned2).thenThrow(toBeThrown);

        assertSame(toBeReturned, testSubject.findFirstHistoryRecord());
        assertSame(toBeReturned2, testSubject.findFirstHistoryRecord());
        try {
            testSubject.findFirstHistoryRecord();
            fail("Should have blown up");
        } catch (RuntimeException e) {
            assertSame(toBeThrown, e);
        }
    }

    @Test
    public void spyOnClassForVerification() throws IOException {
        String contents = "a message";
        ByteArrayInputStream byteArrayInputStream = spy(new ByteArrayInputStream(contents.getBytes()));

        String returnValue = testSubject.readFully(byteArrayInputStream);

        assertEquals(contents, returnValue);
        verify(byteArrayInputStream).close();
    }

    @Test
    public void spyOnClassForStubbing() throws IOException {
        String contents = "a message";
        ByteArrayInputStream byteArrayInputStream = spy(new ByteArrayInputStream(contents.getBytes()));
        IOException toBeThrown = new IOException();
        doThrow(toBeThrown).when(byteArrayInputStream).close();

        try {
            testSubject.readFully(byteArrayInputStream);
            fail("Should have blown up");
        } catch (IOException e) {
            assertSame(toBeThrown, e);
        }
    }
}