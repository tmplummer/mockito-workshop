package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;
import com.carfax.mockito.workshop.helperclasses.HistoryRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ClassWithDatesTest {
    @Mock
    private FakeDao dao;
    @Captor
    private ArgumentCaptor<HistoryRecord> historyRecordArgumentCaptor;
    private ClassWithDates testSubject = new ClassWithDates();
    private Date dateBeforeTest = new Date();

    @Test
    public void storeTheDateStoresTheCurrentDate() {
        testSubject.storeDate(dao);

        Date dateAfterCall = new Date();
        verify(dao).storeDate(dateBetween(dateBeforeTest, dateAfterCall));
    }

    @Test
    public void storeHistoryRecordWithTheCorrectLastModifiedDateWithArgumentCaptor() {
        testSubject.storeHistoryRecord(dao);

        ArgumentCaptor<HistoryRecord> argumentCaptor = ArgumentCaptor.forClass(HistoryRecord.class);
        verify(dao).storeRecord(argumentCaptor.capture());
        HistoryRecord capturedHistoryRecord = argumentCaptor.getValue();
        assertEquals("vin", capturedHistoryRecord.getVin());
        assertEquals(new Date(0), capturedHistoryRecord.getTranDate());
        assertEquals("description", capturedHistoryRecord.getDescription());
        assertTrue(dateBeforeTest.compareTo(capturedHistoryRecord.getLastModifiedDate()) <= 0);
        assertTrue(new Date().compareTo(capturedHistoryRecord.getLastModifiedDate()) >= 0);
    }

    @Test
    public void storeHistoryRecordWithTheCorrectLastModifiedDateWithArgumentCaptorCreatedDueToAnnotation() {
        testSubject.storeHistoryRecord(dao);

        verify(dao).storeRecord(historyRecordArgumentCaptor.capture());
        HistoryRecord capturedHistoryRecord = historyRecordArgumentCaptor.getValue();
        assertEquals("vin", capturedHistoryRecord.getVin());
        assertEquals(new Date(0), capturedHistoryRecord.getTranDate());
        assertEquals("description", capturedHistoryRecord.getDescription());
        assertTrue(dateBeforeTest.compareTo(capturedHistoryRecord.getLastModifiedDate()) <= 0);
        assertTrue(new Date().compareTo(capturedHistoryRecord.getLastModifiedDate()) >= 0);
    }

    @Test
    public void storeHistoryRecordWithTheCorrectLastModifiedDateWithCustomArgumentMatcher() {
        testSubject.storeHistoryRecord(dao);

        HistoryRecord expectedHistoryRecord = new HistoryRecord("vin", new Date(0), "description");
        verify(dao).storeRecord(historyRecordWithCorrectLastModifiedDate(expectedHistoryRecord));
    }

    private HistoryRecord historyRecordWithCorrectLastModifiedDate(HistoryRecord historyRecord) {
        return argThat(argument -> {
            Date currentDate = new Date();
            Date lastModifiedDate = argument.getLastModifiedDate();
            if (dateBeforeTest.after(lastModifiedDate) || currentDate.before(lastModifiedDate)) {
                return false;
            } else {
                historyRecord.setLastModifiedDate(lastModifiedDate);
                return historyRecord.equals(argument);
            }
        });
    }

    private Date dateBetween(Date dateBeforeCall, Date dateAfterCall) {
        return argThat(argument ->
                (argument.equals(dateBeforeCall)
                        || argument.after(dateBeforeCall)) &&
                        (argument.equals(dateAfterCall) || argument.before(dateAfterCall)));
    }

}