package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.VerificationCollector;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LazyVerificationExampleTest {
    @Rule
    public VerificationCollector collector = MockitoJUnit.collector();
    private FakeDao dao = mock(FakeDao.class);
    private LazyVerificationExample testSubject = new LazyVerificationExample(dao);

    @Test
    public void test() {
        Date date1 = new Date(1);
        Date date2 = new Date();
        Date date3 = new Date(100000000000L);

        testSubject.storeMany(date1, date2, date3);

        verify(dao).storeDate(date1);
        verify(dao).storeDate(date2);
        verify(dao).storeDate(date3);
    }

}