package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FinalClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class FinalExamplesTest {
    @Mock
    private FinalClass finalClass;
    @InjectMocks
    private FinalExamples testSubject = new FinalExamples();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getSomethingFromFinalClass() {
        String expectedValue = "something different";
        when(finalClass.getSomething()).thenReturn(expectedValue);

        assertEquals(expectedValue, testSubject.getSomethingFromFinalClass());
    }

}