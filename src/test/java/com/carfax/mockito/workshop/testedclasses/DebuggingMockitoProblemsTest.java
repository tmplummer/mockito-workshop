package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.FakeDao;
import com.carfax.mockito.workshop.helperclasses.FinalClass;
import com.carfax.mockito.workshop.helperclasses.HistoryRecord;
import org.junit.Test;
import org.mockito.InOrder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class DebuggingMockitoProblemsTest {
    private FinalClass finalClass = mock(FinalClass.class);
    private FakeDao dao = mock(FakeDao.class);
    private DebuggingMockitoProblems testSubject = new DebuggingMockitoProblems(finalClass);

    @Test
    public void storeTheRecordWithTheCorrectDescription() {
        Date tranDate = new Date();
        HistoryRecord record = new HistoryRecord("vin", tranDate, "old description");

        testSubject.storeRecord(dao, record);

        verify(dao).storeRecord(new HistoryRecord("vin", tranDate, "new description"));
    }

    @Test
    public void storeRecordWithOriginalDescriptionThenUpdateRecordWithNewDescription() {
        testSubject.storeThenUpdateRecord(dao);

        verify(dao).storeRecord(new HistoryRecord("vin", new Date(0), "description"));
        verify(dao).update(new HistoryRecord("vin", new Date(0), "new description"));
    }

    @Test
    public void storeRecordWithOriginalDescriptionThenUpdateRecordWithNewDescription2() {
        doAnswer(invocation -> {
            assertEquals(new HistoryRecord("vin", new Date(0), "description"), invocation.getArgument(0));
            return null;
        }).when(dao).storeRecord(any(HistoryRecord.class));
        doAnswer(invocation -> {
                assertEquals(new HistoryRecord("vin", new Date(0), "new description"), invocation.getArgument(0));
                return null;
        }).when(dao).update(any(HistoryRecord.class));

        testSubject.storeThenUpdateRecord(dao);

        InOrder inOrder = inOrder(dao);
        inOrder.verify(dao).storeRecord(any(HistoryRecord.class));
        inOrder.verify(dao).update(any(HistoryRecord.class));
    }

    @Test
    public void improperUseOfMockito() {
        testSubject.doSomething();

        verify(finalClass).doSomething("hello", anyInt());
    }

    @Test
    public void anotherImproperUseOfMockito() {
        ArrayList<String> listToSpyOn = spy(new ArrayList<>());
        String expectedValue = "something";
        when(listToSpyOn.get(0)).thenReturn(expectedValue);

        assertEquals(expectedValue, listToSpyOn.get(0));
    }

    @Test
    public void mistakeUsingMockito() {
        String expectedValue = "something different";
        when(finalClass.getSomething());

        assertEquals(expectedValue, testSubject.getSomethingFromFinalClass());
    }

    @Test
    public void oneWeirdProblem() {
        String expectedValue = "something different";
        when(finalClass.getSomething()).thenReturn(expectedValue);
        when(finalClass.getSomethingElse()).thenReturn(finalClass.getSomething());

        assertEquals(expectedValue, testSubject.getSomethingFromFinalClass());
        assertEquals(expectedValue, testSubject.getSomethingElseFromFinalClass());
    }


    @Test
    public void invalidExceptions() {
        when(finalClass.getSomething()).thenThrow(new IOException());

        testSubject.getSomethingFromFinalClass();
    }

}