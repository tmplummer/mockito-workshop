package com.carfax.mockito.workshop.testedclasses;

import com.carfax.mockito.workshop.helperclasses.OrderThing;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import static org.mockito.Mockito.*;

public class OrderExamplesTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);
    @Mock
    private OrderThing thing1;
    @Mock
    private OrderThing thing2;
    @Mock
    private OrderThing thing3;
    private OrderExamples testSubject = new OrderExamples();

    @Test
    public void doThingsInTheCorrectOrder() {
        testSubject.doThingsInTheCorrectOrder(thing1, thing2, thing3);

        InOrder inOrder = inOrder(thing1, thing2, thing3);
        inOrder.verify(thing1).first();
        inOrder.verify(thing1).second();
        inOrder.verify(thing1).third();
        inOrder.verify(thing2).first();
        inOrder.verify(thing2).second();
        inOrder.verify(thing2).third();
        inOrder.verify(thing3).first();
        inOrder.verify(thing3).second();
        inOrder.verify(thing3).third();
    }

    @Test
    public void verifyOnlyWhatYouWantInOrder() {
        testSubject.doThingsInTheCorrectOrder(thing1, thing2, thing3);

        InOrder inOrder = inOrder(thing1, thing2, thing3);
        inOrder.verify(thing1).first();
        inOrder.verify(thing2).second();
        inOrder.verify(thing3).third();
        verify(thing1).second();
        verify(thing1).third();
        verify(thing2).first();
        verify(thing2).third();
        verify(thing3).first();
        verify(thing3).second();
    }

    @Test
    public void verifyOnlyWhatYouWantInOrderForDifferentNeeds() {
        testSubject.doThingsInTheCorrectOrder(thing1, thing2, thing3);

        InOrder inOrder = inOrder(thing1, thing2, thing3);
        inOrder.verify(thing1).first();
        inOrder.verify(thing1).third();
        inOrder.verify(thing2).first();
        inOrder.verify(thing2).third();
        inOrder.verify(thing3).first();
        inOrder.verify(thing3).third();
    }

    @Test
    public void doThingsInTheCorrectButDifferentOrder() {
        testSubject.doThingsInTheCorrectButDifferentOrder(thing1, thing2, thing3);

        InOrder inOrder = inOrder(thing1, thing2, thing3);
        inOrder.verify(thing1).first();
        inOrder.verify(thing2).first();
        inOrder.verify(thing3).first();
        inOrder.verify(thing1).second();
        inOrder.verify(thing2).second();
        inOrder.verify(thing3).second();
        inOrder.verify(thing1).third();
        inOrder.verify(thing2).third();
        inOrder.verify(thing3).third();
    }

    @Test
    public void doThingsInCorrectButRandomOrder() {
        testSubject.doThingsInCorrectButRandomOrder(thing1, thing2, thing3);

        InOrder inOrder1 = inOrder(thing1, thing2, thing3);
        inOrder1.verify(thing1).first();
        inOrder1.verify(thing1).second();
        inOrder1.verify(thing1).third();
        InOrder inOrder2 = inOrder(thing1, thing2, thing3);
        inOrder2.verify(thing2).first();
        inOrder2.verify(thing2).second();
        inOrder2.verify(thing2).third();
        InOrder inOrder3 = inOrder(thing1, thing2, thing3);
        inOrder3.verify(thing3).first();
        inOrder3.verify(thing3).second();
        inOrder3.verify(thing3).third();
    }

    @Test
    public void exploreInOrderGreedyness() {
        testSubject.doThings(thing1);

        InOrder inOrder = inOrder(thing1, thing2, thing3);
        inOrder.verify(thing1).first();
        inOrder.verify(thing1).second();
        // do this instead
//        InOrder inOrder = inOrder(thing1, thing2, thing3);
//        inOrder.verify(thing1, times(2)).first();
//        inOrder.verify(thing1).second();
        // or
//        InOrder inOrder = inOrder(thing1, thing2, thing3);
//        inOrder.verify(thing1, atLeast(1)).first();
//        inOrder.verify(thing1).second();
    }

    @Test
    public void verifyNoInteractionsWithThing() {
        testSubject.doNothingWith(thing1);

        verifyZeroInteractions(thing1);
    }


    @Test
    public void verifyNoMoreInteractionsWithThings() {
        testSubject.doOnlyFirstWith(thing1, thing2, thing3);

        verify(thing1).first();
        verify(thing2).first();
        verify(thing3).first();
        verifyNoMoreInteractions(thing1, thing2, thing3);
    }

}